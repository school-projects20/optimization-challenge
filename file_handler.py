from structures import Instance, Voiture
import os


def read_instance_file(id_instance: int):
    with open(f"checker/cs{id_instance}.txt") as instance:
        data = instance.read()
        instance.close()
    start, options, liste_voitures = data.split("\n\n")
    ligne_options, ligne_voitures = start.split("\n")
    _, nb_options = ligne_options.split()
    nb_options = int(nb_options)
    _, nb_voitures = ligne_voitures.split()
    nb_voitures = int(nb_voitures)
    options_lines = options.split("\n")
    voitures_lines = liste_voitures.split("\n")
    options_info = []
    for i in range(nb_options):
        ratio_num, ratio_denom, poids, *_ = options_lines[i+1].split()
        options_info.append({"id" : i+1, "ratio_num" : int(ratio_num), "ratio_denom" : int(ratio_denom), "poids" : int(poids)})

    liste_voitures = []
    for i in range(nb_voitures):
        id_voiture, *options = voitures_lines[i+1].split()
        voiture = Voiture(id=int(id_voiture), options=[int(options[j]) for j in range(nb_options)])
        liste_voitures.append(voiture)

    instance = Instance(id=id_instance, nb_options=nb_options, nb_voitures=nb_voitures, options_info=options_info, liste_voitures=liste_voitures)
    return instance

def delete_old_solution(instance, poids):
    os.remove(f"best_results/res_{instance.id}_{poids}.txt")

def write_solution(instance):
    with open(f"best_results/res_{instance.id}_{instance.best_solution_cost}.txt", "w") as output_file:
        output_file.write("EQUIPE Chimère Blaireau-python\n")
        output_file.write(f"INSTANCE {instance.id}\n")
        for numero in instance.best_solution:
            output_file.write(f"{numero} ")
    with open("best.txt", "r") as best:
        best_values = best.readlines()
        best.close
    if instance.id == 15:
        best_values[instance.id-1] = str(instance.best_solution_cost)
    else:
        best_values[instance.id-1] = str(instance.best_solution_cost)+"\n"
    with open("best.txt", "w") as best:
        best.writelines(best_values)
        best.close()


def read_solution(filename):
    with open(filename, "r") as solution_file:
        solution = solution_file.read()
        solution_file.close()
        *_, liste_voitures = solution.split("\n")
        liste_voitures_separees = liste_voitures.split()
        liste_voitures_separees = list(map(int, liste_voitures_separees))
        return liste_voitures_separees