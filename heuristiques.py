from structures import *
from file_handler import *
import random
from checker import checker

def double_random_exchange(instance):
    solution = instance.best_solution
    i = random.randrange(instance.nb_voitures)
    j = random.randrange(instance.nb_voitures)
    solution[i], solution[j] = solution[j], solution[i]
    cost = checker(instance, solution)
    old_cost = instance.best_solution_cost
    if cost < old_cost:
        delete_old_solution(instance, old_cost)
        instance.best_solution_cost = cost
        write_solution(instance)
        print("For ", instance.id, " found ", cost)
    elif cost == old_cost:
        pass
    else:
        solution[i], solution[j] = solution[j], solution[i]

def single_random_one_exchange(instance):
    solution = instance.best_solution
    to_change = []
    for option in range(0, instance.nb_options):
        ratio_num = instance.options_info[option]["ratio_num"]
        ratio_denom = instance.options_info[option]["ratio_denom"]
        for start_window in range(0, len(solution)):
            end_window = start_window + ratio_denom
            compte = 0
            to_change_temp = []
            for j in solution[start_window : min(end_window, len(solution))]:
                if instance.liste_voitures[j-1].options[option]:
                    to_change_temp.append(j-1)
                    compte += 1
            if compte > ratio_num:
                to_change += to_change_temp
    for _ in range(1000):
        i = random.randrange(instance.nb_voitures)
        j = to_change[random.randrange(len(to_change))]
        solution[i], solution[j] = solution[j], solution[i]
        cost = checker(instance, solution)
        old_cost = instance.best_solution_cost
        if cost < old_cost:
            delete_old_solution(instance, old_cost)
            instance.best_solution_cost = cost
            write_solution(instance)
            print("For ", instance.id, " found ", cost)
            break
        elif cost == old_cost:
            pass
        else:
            solution[i], solution[j] = solution[j], solution[i]

def double_random_one_exchange(instance):
    solution = instance.best_solution
    to_change = []
    for option in range(0, instance.nb_options):
        ratio_num = instance.options_info[option]["ratio_num"]
        ratio_denom = instance.options_info[option]["ratio_denom"]
        for start_window in range(0, len(solution)):
            end_window = start_window + ratio_denom
            compte = 0
            to_change_temp = []
            for j in solution[start_window : min(end_window, len(solution))]:
                if instance.liste_voitures[j-1].options[option]:
                    to_change_temp.append(j-1)
                    compte += 1
            if compte > ratio_num:
                to_change += to_change_temp
    for _ in range(1000):
        i = to_change[random.randrange(len(to_change))]
        j = random.randrange(instance.nb_voitures)
        while i == j:
            j = random.randrange(instance.nb_voitures)
        k = random.randrange(instance.nb_voitures)
        while i == k or j == k:
            k = random.randrange(instance.nb_voitures)
        solution[i], solution[j], solution[k] = solution[j], solution[k], solution[i]
        cost = checker(instance, solution)
        old_cost = instance.best_solution_cost
        if cost < old_cost:
            delete_old_solution(instance, old_cost)
            instance.best_solution_cost = cost
            write_solution(instance)
            print("For ", instance.id, " found ", cost)
            break
        elif cost == old_cost:
            pass
        else:
            solution[j], solution[k], solution[i] = solution[i], solution[j], solution[k]


def quadruple_random_one_exchange(instance):
    solution = instance.best_solution
    to_change = []
    for option in range(0, instance.nb_options):
        ratio_num = instance.options_info[option]["ratio_num"]
        ratio_denom = instance.options_info[option]["ratio_denom"]
        for start_window in range(0, len(solution)):
            end_window = start_window + ratio_denom
            compte = 0
            to_change_temp = []
            for j in solution[start_window : min(end_window, len(solution))]:
                if instance.liste_voitures[j-1].options[option]:
                    to_change_temp.append(j-1)
                    compte += 1
            if compte > ratio_num:
                to_change += to_change_temp
    for _ in range(1000):
        i = to_change[random.randrange(len(to_change))]
        j = random.randrange(instance.nb_voitures)
        while i == j:
            j = random.randrange(instance.nb_voitures)
        k = random.randrange(instance.nb_voitures)
        while i == k or j == k:
            k = random.randrange(instance.nb_voitures)
        l = random.randrange(instance.nb_voitures)
        while i == l or j == l or k == l:
            l = random.randrange(instance.nb_voitures)
        m = random.randrange(instance.nb_voitures)
        while i == m or j == m or k == m or l == m:
            m = random.randrange(instance.nb_voitures)
        solution[i], solution[j], solution[k], solution[l], solution[m] = solution[j], solution[k],solution[l], solution[m], solution[i]
        cost = checker(instance, solution)
        old_cost = instance.best_solution_cost
        if cost < old_cost:
            delete_old_solution(instance, old_cost)
            instance.best_solution_cost = cost
            write_solution(instance)
            print("For ", instance.id, " found ", cost)
            break
        elif cost == old_cost:
            pass
        else:
            solution[j], solution[k],solution[l], solution[m], solution[i] = solution[i], solution[j], solution[k], solution[l], solution[m]