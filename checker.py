def checker(instance, solution):
    poids_toutes_options = 0
    for option in range(0, instance.nb_options):
        poids = instance.options_info[option]["poids"]
        ratio_num = instance.options_info[option]["ratio_num"]
        ratio_denom = instance.options_info[option]["ratio_denom"]
        for start_window in range(0, len(solution)):
            end_window = start_window + ratio_denom
            compte = 0
            for j in solution[start_window : min(end_window, len(solution))]:
                if instance.liste_voitures[j-1].options[option]:
                    compte += 1
            poids_toutes_options = poids_toutes_options +  poids * max(0, compte - ratio_num)

    return poids_toutes_options