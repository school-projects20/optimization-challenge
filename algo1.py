from structures import *
INFINI = 100000000

def algo1(instance):
    nb_voitures = instance.nb_voitures
    liste_voiture_ajoutee = [False]*nb_voitures
    solution = [0]
    liste_voiture_ajoutee[0] = True
    poids_total = 0
    for _ in range(1, nb_voitures):
        meilleure_voiture = -1
        meilleure_valeur = INFINI
        for id in range(0, nb_voitures):
            if not liste_voiture_ajoutee[id]:
                solution.append(id)
                valeur = evaluer_valeur_ajout(instance, solution)
                solution.pop()
                if valeur < meilleure_valeur:
                    meilleure_valeur = valeur
                    meilleure_voiture = id
        poids_total += meilleure_valeur
        liste_voiture_ajoutee[meilleure_voiture] = True
        solution.append(meilleure_voiture)
    instance.best_solution = list(map(lambda x: x+1, solution))
    instance.best_solution_cost = checker(instance, instance.best_solution)


def evaluer_valeur_ajout(instance, solution):
    poids_toutes_options = 0
    for option in range(0, instance.nb_options):
        poids = instance.options_info[option]["poids"]
        ratio_num = instance.options_info[option]["ratio_num"]
        ratio_denom = instance.options_info[option]["ratio_denom"]
        compte = 0
        for j in solution[-1-ratio_denom:-1]:
            if instance.liste_voitures[j].options[option]:
                compte += 1
        poids_toutes_options = poids_toutes_options +  poids * max(0, compte - ratio_num)

    return poids_toutes_options