from checker import checker

class Instance:
    def __init__(self, id, nb_options, nb_voitures, options_info, liste_voitures, best_solution = None, best_solution_cost = None):
        self.id = id
        self.nb_options = nb_options
        self.nb_voitures = nb_voitures
        self.options_info = options_info
        self.liste_voitures = liste_voitures
        self.best_solution = best_solution if best_solution is not None else range(nb_voitures)
        self.best_solution_cost = best_solution_cost if best_solution_cost is not None else checker(self, self.best_solution)

class Voiture:
    def __init__(self, id, options):
        self.id = id
        self.options = options