from os import listdir

from file_handler import *
from algo1 import *
from heuristiques import *
from multiprocessing import Pool, Process

def algo1_generate(i):
    algo1(liste_instances[i])
    write_solution(liste_instances[i])
    print(f"File {i+1} created from algo1.")

def first_function(instance):
    while instance.best_solution_cost != 0:
        double_random_exchange(instance)
    print("Already perfect for ", instance.id)

def second_function(instance):
    while instance.best_solution_cost != 0:
        single_random_one_exchange(instance)
    print("Already perfect for ", instance.id)

def third_function(instance):
    while instance.best_solution_cost != 0:
        double_random_one_exchange(instance)
    print("Already perfect for ", instance.id)

def fifth_function(instance):
    while instance.best_solution_cost != 0:
        quadruple_random_one_exchange(instance)
    print("Already perfect for ", instance.id)

def all_of_them(instance):
    while instance.best_solution_cost != 0:
        single_random_one_exchange(instance)
        double_random_one_exchange(instance)
        quadruple_random_one_exchange(instance)
    print("Already perfect for ", instance.id)

if __name__ == "__main__":
    liste_instances = []
    for i in range(1, 16):
        instance = read_instance_file(i)
        liste_instances.append(instance)

    files = listdir("best_results/")
    if files == []:
        with Pool(15) as p:
            p.map(algo1_generate, range(15))
    with open("best.txt") as best_out:
        liste_best_values = best_out.read().split()
        best_out.close()
    liste_best_values = [int(liste_best_values[i]) for i in range(15)]
    print(liste_best_values)
    for i in range(15):
        cost = liste_best_values[i]
        liste_instances[i].best_solution_cost = cost
        liste_instances[i].best_solution = read_solution(f"best_results/res_{i+1}_{cost}.txt")
    choice = 6
    with Pool(15) as p:
        if choice == 1:
            p.map(first_function, liste_instances)
        elif choice == 2:
            p.map(second_function, liste_instances)
        elif choice == 3:
            p.map(third_function, liste_instances)
        elif choice == 5:
            p.map(fifth_function, liste_instances)
        elif choice == 6:
            p.map(all_of_them, liste_instances)